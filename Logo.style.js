import reactCSS from 'reactcss';
export const styles = reactCSS({
  'default': {
    LogoContainer: {
      display: 'block'
    },
    LogoContainerFooter: {
      width: '15%',
      display: 'inline-block',
      margin: '10px auto 0'
    }
  }
});
